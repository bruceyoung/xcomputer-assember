import utils.DataUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InstrctionsPretreater {
    public Map<String,Integer> labelMap = new HashMap<>();
    public Map<String,Integer> wordMap = new HashMap<>();

    public List<String> pretreat(List<String> validLines){
        int instructionNum = 0;
        String binCodes = "";
        String hexCodes = "";
        List<String> instructionLines = new ArrayList<>();
        for(String line:validLines){

            if(line.endsWith(":")){//label
                labelMap.put(line.replace(":",""),instructionNum);//label point to next instruction position
                continue;
            }
            if(line.contains(".word")){//word 映射
                parseWord(line);
                continue;
            }
            //Normal Instruction
            instructionLines.add(line);
            instructionNum ++;
        }

        return instructionLines;
    }

    private void parseWord(String content){
        String[] segments = content.trim().split(" ");
        wordMap.put(segments[0], Integer.parseInt(segments[2]));
    }
}
