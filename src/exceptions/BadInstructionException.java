package exceptions;

/**
 * @ClassName: BadInstructionException
 * @Description: TODO
 * @author: DJ008854
 * @date: 2020年10月27日 17:25
 */
public class BadInstructionException extends RuntimeException {
    public BadInstructionException() {
    }

    public BadInstructionException(String message) {
        super(message);
    }
}
