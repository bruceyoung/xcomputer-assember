//label
foodX .word 0
foodY .word 1
snakeLenPos .word 3
stepX .word 4
stepY .word 5
direction .word 6
snakeBasePos .word 100
i .word 7
Init:
	//init snake head
	movi r0,4
	movi r1,snakeBasePos
	sw r0,0(r1)//设置蛇头初始X位置
	sw r0,1(r1)//设置蛇头初始Y位置
	//r0,r1使用完毕
	movi r0,2
	movi r1,snakeLenPos
	sw r0,0(r1)//设置蛇长度
	//r0,r1使用完毕


	//init step and direction
	movi r0,1
	movi r1,stepX
	sw r0,0(r1)//设置X方向速度为1
	movi r0,0
	movi r1,stepY
	sw r0,0(r1)//设置y方向速度为0

	movi r0,1//1是向右转
	movi r1,direction
	sw r0,0(r1)//设置方向


CalI:
    movi r0,snakeLenPos
    lw r1,0(r0)//r1->蛇长snakeLen
    add r1,r1,r1//r1->2*snakeLen
    movi r2,snakeBasePos//r2->snakeBasePos
    addi r2,r2,2//r2->snakeBasePos+2
    sub r2,r2,r1//r2->snakeBasePos+2-2*snakeLen
    movi r0,i
    sw r2,0(r0)//把末尾索引存到i中


UpdateSnakeBody:
    //r0存放索引地址，r1存放索引值
    movi r0,i
    lw r1,0(r0)//读出索引i位置的值
    movi r2,snakeBasePos
    beq r1,r2,UpdateSnakeHead//遇到蛇头

    lw r3,2(r1)//读取m[i+2]位置的值，也就是上一个点的x
    sw r3,0(r1)//把m[i+2]的值存入当前m[i]中，更新X
    lw r3,3(r1)//读取m[i+3]位置的值，也就是上一个点的
    sw r3,1(r1)//把m[i+3]的值存入当前m[i+1]中，更新y


    addi r1,r1,2//ivalue=ivalue+2
    sw r1,0(r0)
    jump UpdateSnakeBody

UpdateSnakeHead:
    movi r0,snakeBasePos
    lw r1,0(r0)
    addi r1,r1,1
    sw r1,0(r0)
CLENSCREEN:
    movi r4,9
Draw:
    movi r0,snakeBasePos
    movi r1,i
    sw r0,0(r1)//蛇头x位置存入索引
DrawNext:
    //r1表示索引地址
    movi r1,i
    //r0表示当前索引值
    lw r0,0(r1)//读取当前索引

    lw r2,0(r0)//读取x
    mov r5,r2//设置cols
    lw r2,1(r0)//读取y
    mov r4,r2//设置rows
    movi r6,1//驱动显示
    movi r6,0//驱动显示

    movi r2,snakeLenPos//
    lw r2,0(r2)//读取蛇长snakeLen
    movi r3,1
    sub r2,r2,r3//r2->snakeLen-1
    add r2,r2,r2//r2->2*(snakeLen-1)
    movi r3,snakeBasePos
    sub r3,r3,r0
    beq r2,r3,Loop
    movi r2,2
    sub r0,r0,r2//索引-2
    sw r0,0(r1)//更新索引
    jump DrawNext




Loop:
    jump UpdateSnakeBody

//
//
//
//    mov r0,i//当前蛇身索引
//    mov r1,snakeLen
//    lw r2,(0)r1//读取当前蛇长
//    beq r1,r2,UpdateSnakeHead//如果当前索引等于蛇长，则跳转到设置蛇头
//    //r0,r1,r2使用完毕
//    mov r0,snakeBase
//    sw r1,0(r0)//把
//
//UpdateSnakeHead:
//    mov r0,snakeBase
//	lw r1,0(r0)//读出snake头的X值
//    addi r1,r1,stepX//按速度增长
//    sw r1,0(r0)//写回内存
//    lw r1,1(r0)//读出snake头的Y值
//    addi r1,r1,stepY//按速度增长
//    sw r1,1(r0)//写回内存
//
//
//
//Show:
//    add r0,r0,1 			#next x
//	load r2,r0
//	store r1,r0
//	mov r1,r2
//	load r3,i
//	add r3,r3,1
//	store r3,i
//	bne r3,0,Show
//	jump UpdateSnakeBody



